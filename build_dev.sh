#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ -f "${CURRENT_DIR}/.env.local" ]]; then
  sed -i "s|^APP_ENV=prod$|APP_ENV=dev|g" "${CURRENT_DIR}/.env.local"
fi

composer install

yarn install

yarn encore dev --watch
