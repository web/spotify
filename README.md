# Spotify playlist generator

Create fresh playlists from selected top played artists or existing playlists.

Playlists are generated as public Spotify playlists, with 50 tracks (~3 hours), from your personal Spotify recommendations.

## Generate fresh playlist form artists

From the list of your top played artists.

-   pick one or several artists in list
-   click on "generate"
-   a fresh playlist is generated with recommendations from each artist in pick ones

You can choose if you want all recommendations, or only the ones with picked artists.

A direct button can help to randomly pick artists.

## Generate fresh playlist from existing playlists

-   pick one or several playlist in list
-   click on "generate"
-   a fresh playlist is generated with recommendations from each track in picked playlists

You can choose if you want all recommendations, or only the ones with artists in picked playlists.
