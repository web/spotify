<?php

namespace App\Service;

use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\Session;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SpotifyApiService
{
    private $router;

    private $requestStack;

    private $params;

    public const SPOTIFY_REQUIRED_SCOPES = [
        'playlist-read-private',
        'playlist-modify-private',
        'playlist-modify-public',
        'user-read-private',
        'user-top-read',
        'user-read-currently-playing',
    ];

    public function __construct(UrlGeneratorInterface $router, RequestStack $requestStack, ContainerBagInterface $params)
    {
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->params = $params;
    }

    public function createSession(): Session
    {
        $spotifyClientId = $this->params->get('spotify.client_id');
        $spotifyClientSecret = $this->params->get('spotify.client_secret');
        $spotifyRedirectUri = $this->params->get('spotify.redirect_uri');

        return new Session($spotifyClientId, $spotifyClientSecret, $spotifyRedirectUri);
    }

    public function createApi(): SpotifyWebAPI
    {
        $session = $this->requestStack->getSession();

        $session->set('accessToken', $session->get('accessToken', ''));
        $session->set('refreshToken', $session->get('refreshToken', ''));

        if (!$session->get('accessToken', '')) {
            header('Location: ' . $this->router->generate('authenticate'));
            die();
        }

        $spotifySession = $this->createSession();

        // Use previously requested tokens fetched from session
        if ($session->get('accessToken', '')) {
            $spotifySession->setAccessToken($session->get('accessToken', ''));
            $spotifySession->setRefreshToken($session->get('refreshToken', ''));
        } else {
            // Or request a new access token
            $spotifySession->refreshAccessToken($session->get('refreshToken', ''));
        }

        $options = [
            'scope' => self::SPOTIFY_REQUIRED_SCOPES,
            'auto_refresh' => true,
        ];

        $api = new SpotifyWebAPI($options, $spotifySession);

        // Save new tokens, they might have been updated
        $session->set('accessToken', $spotifySession->getAccessToken());
        $session->set('refreshToken', $spotifySession->getRefreshToken());

        return $api;
    }
}
