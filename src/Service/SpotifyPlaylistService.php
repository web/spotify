<?php

namespace App\Service;

use DateTime;
use SpotifyWebAPI\SpotifyWebAPIException;

class SpotifyPlaylistService
{
    public const TOP_ARTISTS_COUNT = 40;
    public const PLAYLISTS_COUNT = 24;

    public const TRACKS_COUNT_IN_SHORT_PLAYLIST = 50;
    public const TRACKS_COUNT_IN_LONG_PLAYLIST = 100;

    public function printLog($message = '')
    {
        if (false) {
            error_log($message);
        }
    }

    public function generatePlaylistName($prefix)
    {
        $now = new DateTime();
        $name = $prefix . ' [' . $now->format('Y-m-d H:i:s') . ']';

        return $name;
    }

    public function createEmptyPlaylist($api, $playlistName = 'Auto playlist')
    {
        $name = $this->generatePlaylistName($playlistName);
        $this->printLog('Create new playlist "' . $name . '".');
        $playlist = $api->createPlaylist([
            'name' => $name,
        ]);

        return $playlist;
    }

    public function pickRandomTracksFromArray($trackIds, $count = 0)
    {
        $trackIds = \array_keys($trackIds);
        $this->printLog('=> Got ' . \count($trackIds) . ' unique tracks.');
        \shuffle($trackIds);

        if (!$count) {
            return $trackIds;
        }

        $pickedTrackIds = \array_slice($trackIds, 0, $count);
        $this->printLog('Keep ' . \count($pickedTrackIds) . ' random tracks.');

        return $pickedTrackIds;
    }

    public function getRandomArtistsFromTopArtists($api, $countInTopArtists, $countInLessTopArtists, $limitBetweenTopAndLessTop)
    {
        $top = $api->getMyTop(
            'artists',
            [
                'limit' => $limitBetweenTopAndLessTop,
                'time_range' => 'short_term'
            ]
        )->items;
        shuffle($top);
        $pickedTop = array_slice($top, 0, $countInTopArtists);

        $lessTop = $api->getMyTop(
            'artists',
            [
                'limit' => $limitBetweenTopAndLessTop,
                'offset' => $limitBetweenTopAndLessTop,
                'time_range' => 'short_term'
            ]
        )->items;
        shuffle($lessTop);
        $pickedLessTop = array_slice($lessTop, 0, $countInLessTopArtists);

        $randomlySelected = [];
        foreach ($pickedTop as $artist) {
            $randomlySelected[] = $artist->id;
        }
        foreach ($pickedLessTop as $artist) {
            $randomlySelected[] = $artist->id;
        }

        return $randomlySelected;
    }

    public function createPlaylistWithRandomTracks($api, $trackIds, $count = 50)
    {
        $playlist = $this->createEmptyPlaylist($api, 'Fresh playlist');
        $pickedTrackIds = $this->pickRandomTracksFromArray($trackIds, $count);
        $api->replacePlaylistTracks($playlist->id, $pickedTrackIds);

        return $api->getPlaylist($playlist->id);
    }

    public function createPlaylistFromDailyMixesTracks($api, $count = 50)
    {
        // Get daily mix playlists from user library
        $userPlaylists = $this->getAllPlaylistsFromUserId($api, $api->me()->id);
        $dailyMixPlaylistIds = [];
        foreach ($userPlaylists as $userPlaylist) {
            $regex = '/^Daily Mix [1-6]$/';
            if (\preg_match($regex, $userPlaylist->name)) {
                $dailyMixPlaylistIds[] = $userPlaylist->id;
            }
        }

        // Get all tracks in daily mix playlists
        $trackIds = [];
        foreach ($dailyMixPlaylistIds as $dailyMixPlaylistId) {
            $dailyMixPlaylist = $api->getPlaylist($dailyMixPlaylistId);
            $tracks = $dailyMixPlaylist->tracks->items;
            foreach ($tracks as $track) {
                $trackId = $track->track->id;
                $trackIds[$trackId] = $trackId;
            }
        }

        $pickedTrackIds = $this->pickRandomTracksFromArray($trackIds, $count);

        $playlist = $this->createEmptyPlaylist($api, 'Daily Mixes');
        $api->replacePlaylistTracks($playlist->id, $pickedTrackIds);

        return $api->getPlaylist($playlist->id);
    }

    public function createPlaylistTambouilleMix($api, $count = 50)
    {
        // Get specific playlists
        $wantedPlaylistsNames = [
            'Daily Mix 1',
            'Daily Mix 2',
            'Radar des sorties',
            'Discover Weekly', # "Découvertes de la semaine"
            'Your Time Capsule', # "Votre Capsule Temporelle"
            'On Repeat', # "En boucle"
            'Repeat Rewind', # "En boucle : flashback"
        ];

        $userPlaylists = $this->getAllPlaylistsFromUserId($api, $api->me()->id);
        $userPlaylistsByName = [];
        foreach ($userPlaylists as $userPlaylist) {
            $userPlaylistsByName[$userPlaylist->name] = $userPlaylist->id;
        }

        // Get all tracks, pick some and get recommandations from playlists
        $trackIds = [];
        foreach ($wantedPlaylistsNames as $playlistName) {
            if (\array_key_exists($playlistName, $userPlaylistsByName)) {
                $playlistId = $userPlaylistsByName[$playlistName];
                $selectedPlaylist = $api->getPlaylist($playlistId);

                $tracks = $selectedPlaylist->tracks->items;
                foreach ($tracks as $track) {
                    $trackId = $track->track->id;
                    $selectedPlaylistTracks[$trackId] = $trackId;
                }

                $tracksCount = \count($selectedPlaylistTracks);

                // Split playlist in two tracks lists
                $splittedTracksListLength = max(intval($count / 5), intval($tracksCount / 5));
                $tracksList1 = $this->pickRandomTracksFromArray($selectedPlaylistTracks, $splittedTracksListLength);
                $tracksList2 = $this->pickRandomTracksFromArray($selectedPlaylistTracks, $splittedTracksListLength);

                // Directly pick first part of this playlist
                foreach ($tracksList1 as $trackId) {
                    $trackIds[$trackId] = $trackId;
                }

                // Get recommandations for other part
                $chunks = \array_chunk($tracksList2, 5);
                foreach ($chunks as $chunk) {
                    try {
                        $recommendations = $api->getRecommendations([
                            'seed_tracks' => $chunk,
                            'limit' => \count($chunk)
                        ]);
                        foreach ($recommendations->tracks as $recommendedTrack) {
                            $trackIds[$recommendedTrack->id] = $recommendedTrack->id;
                        }
                    } catch (SpotifyWebAPIException $e) {
                        // Failed on retrieve recommendations => add seed tracks instead
                        foreach ($chunk as $fallbackTrackId) {
                            $trackIds[$fallbackTrackId] = $fallbackTrackId;
                        }
                    }
                }
            }
        }

        $pickedTrackIds = $this->pickRandomTracksFromArray($trackIds, $count);

        $playlist = $this->createEmptyPlaylist($api, 'Tambouille Mix');
        $api->replacePlaylistTracks($playlist->id, $pickedTrackIds);

        return $api->getPlaylist($playlist->id);
    }

    public function getCreatedPlaylistInformationMessage($playlist)
    {
        $link = '<a href="' . $playlist->external_urls->spotify . '">' . $playlist->name . '</a>';
        $message = 'Ok created new playlist with ' . count($playlist->tracks->items) . ' tracks:<br/>' . $link;

        return $message;
    }

    public function getPlaylistInformationMessage($playlist)
    {
        $link = '<a href="' . $playlist->external_urls->spotify . '">' . $playlist->name . '</a>';
        $message = 'Playlist with ' . count($playlist->tracks->items) . ' tracks:<br/>' . $link;

        return $message;
    }

    public function createDisplayablePlaylist($playlist)
    {
        $output = [
            'id' => $playlist->id,
            'name' => $playlist->name,
            'tracks' => [],
            'artists' => [],
        ];

        $artists = [];

        foreach ($playlist->tracks->items as $item) {
            $track = [
                'id' => $item->track->id,
                'name' => $item->track->name,
                'artists' => [],
            ];

            foreach ($item->track->artists as $artist) {
                $track['artists'][] = [
                    'id' => $artist->id,
                    'name' => $artist->name,
                ];
                $artists[$artist->id] = $artist->name;
            }

            $output['tracks'][] = $track;
        }

        $output['artists'] = array_values($artists);

        return $output;
    }

    public function getAllPlaylistsFromUserId($api, $userId)
    {
        $playlists = [];
        $batchSize = 50;
        $offset = 0;

        $batch = $api->getUserPlaylists($userId, [
            'limit' => $batchSize,
            'offset' => $offset,
        ]);
        while (($offset === 0) or (\count($batch->items) === $batchSize)) {
            if ($offset !== 0) {
                $batch = $api->getUserPlaylists($userId, [
                    'limit' => $batchSize,
                    'offset' => $offset,
                ]);
            }

            foreach ($batch->items as $playlist) {
                $playlists[] = $playlist;
            }

            $offset += $batchSize;
        }

        return $playlists;
    }
}
