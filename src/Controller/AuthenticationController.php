<?php

namespace App\Controller;

use App\Service\SpotifyApiService;
use SpotifyWebAPI\SpotifyWebAPIAuthException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

class AuthenticationController extends AbstractController
{
    private $requestStack;
    private $spotifyApiService;

    public function __construct(RequestStack $requestStack, SpotifyApiService $spotifyApiService)
    {
        $this->requestStack = $requestStack;
        $this->spotifyApiService = $spotifyApiService;
    }

    /**
     * @Route("/auth", name="authenticate")
     */
    public function authenticate(): RedirectResponse
    {
        $session = $this->requestStack->getSession();

        $spotifySession = $this->spotifyApiService->createSession();

        $state = $spotifySession->generateState();

        $session->set('state', $state);

        $options = [
            'scope' => SpotifyApiService::SPOTIFY_REQUIRED_SCOPES,
            'state' => $state,
        ];

        return new RedirectResponse($spotifySession->getAuthorizeUrl($options));
    }

    /**
     * @Route("/callback", name="callback")
     */
    public function callback(Request $request): RedirectResponse
    {
        $session = $this->requestStack->getSession();

        $spotifySession = $this->spotifyApiService->createSession();

        $state = $request->get('state');
        $storedState = $session->get('state', '');

        if ($state !== $storedState) {
            header('Location: ' . $this->generateUrl('authenticate'));
        }

        $code = $request->get('code');
        try {
            $spotifySession->requestAccessToken($code);
        } catch (SpotifyWebAPIAuthException $e) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }

        $session->set('accessToken', $spotifySession->getAccessToken());
        $session->set('refreshToken', $spotifySession->getRefreshToken());

        return new RedirectResponse($this->generateUrl('homepage'));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): RedirectResponse
    {
        $session = $this->requestStack->getSession();

        $session->set('accessToken', '');
        $session->set('refreshToken', '');

        return new RedirectResponse($this->generateUrl('homepage'));
    }
}
