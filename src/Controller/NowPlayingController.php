<?php

namespace App\Controller;

use App\Service\SpotifyApiService;
use SpotifyWebAPI\SpotifyWebAPI;
use SpotifyWebAPI\SpotifyWebAPIException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NowPlayingController extends AbstractController
{
    private SpotifyWebAPI $spotifyApi;

    public function __construct(SpotifyApiService $spotifyApiService)
    {
        $this->spotifyApi = $spotifyApiService->createApi();
    }

    /**
     * @Route("/now-playing", name="now-playing")
     */
    public function index(): Response
    {
        try {
            $me = $this->spotifyApi->me();
        } catch (SpotifyWebAPIException $e) {
            return $this->render('error.html.twig');
        }

        return $this->render('now-playing/index.html.twig', [
            'user' => $me,
        ]);
    }

    /**
     * @Route("/now-playing/data", name="now-playing-data")
     */
    public function update(): JsonResponse
    {
        $nowPlaying = $this->spotifyApi->getMyCurrentTrack();

        return new JsonResponse([
            'playing' => $nowPlaying,
        ]);
    }
}
