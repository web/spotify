var lastFetchedNowPlayingData = "";

function updateNowPlaying() {
    var apiUrl = "now-playing/data";

    var newContent = fetch(apiUrl)
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            if (hasChanged(data)) {
                var nowPlayingBlock = document.getElementById("now-playing");
                newContent = buildNowPlayingBlock(data);
                nowPlayingBlock.innerHTML = newContent;
            }
        })
        .catch(function (err) {
            console.warn("Something went wrong.", err);
            newContent =
                '<div class="alert alert-warning">Error fetching API data.<br/>You may need to logout and refresh page.</div>';
            var nowPlayingBlock = document.getElementById("now-playing");
            nowPlayingBlock.innerHTML = newContent;
        });
}

function hasChanged(data) {
    if (data && data["playing"] && data["playing"]["item"]) {
        var dataAsString = JSON.stringify(data["playing"]["item"]);
        if (dataAsString !== lastFetchedNowPlayingData) {
            lastFetchedNowPlayingData = dataAsString;
            return true;
        }
    }

    return false;
}

function buildNowPlayingBlock(apiResponse) {
    var html = `
  <div class="container-fluid now-playing">
    <div class="row">
      <div class="col-12 col-md-6">
        <div class="now-playing-cover"><img src="COVER_URL" class="img-fluid"></div>
      </div>
      <div class="col-12 col-md-6">
        <div class="now-playing-data">
          <div class="now-playing-title">TITLE</div>
          <div class="now-playing-artists">ARTISTS</div>
        </div>
      </div>
    </div>
  </div>`;

    var nowPlayingData = apiResponse["playing"];

    if (!nowPlayingData) {
        return `
        <div class="container-fluid now-playing">
          <div class="row">
            <div class="col">
              <div class="now-playing-nothing">🔇</div>
            </div>
          </div>
        </div>
      `;
    }

    var item = nowPlayingData["item"];
    var album = item["album"];
    var artists = album["artists"];

    var artistNames = artists.map((artist) => artist["name"]).join(", ");
    var trackName = item["name"];
    var coverUrl = album["images"][0]["url"];

    html = html.replace("TITLE", trackName);
    html = html.replace("ARTISTS", artistNames);
    html = html.replace("COVER_URL", coverUrl);

    return html;
}

if (document.getElementById("now-playing")) {
    updateNowPlaying();
    setInterval(updateNowPlaying, 3000);
}
