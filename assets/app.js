import "./styles/app.scss";

const $ = require("jquery");

require("bootstrap");
require("./js/now-playing.js");

/* pick some random artists */
window.pickRandomArtists = function () {
    var pickedArtistsCount = 6 + Math.floor(Math.random() * 6);
    var artistElements = document.getElementsByClassName("checkbox-artist");
    for (var i = 0; i < pickedArtistsCount; i++) {
        var random = Math.floor(Math.random() * artistElements.length);
        artistElements[random].click();
    }
};

/* get playlists and update form */
window.getPlaylists = function () {
    var containerElement = document.getElementById("user-playlists");
    if (containerElement) {
        var htmlContent = "";

        var apiUrl = "playlists/user";
        fetch(apiUrl)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                const htmlTemplate = `
                    <li class="col">
                        <div class="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            class="custom-control-input"
                            id="check-playlist-PLAYLIST_ID"
                            name="selected-playlist[]"
                            value="PLAYLIST_ID"
                        >
                        <label class="custom-control-label" for="check-playlist-PLAYLIST_ID">
                            <span class="spotify-image"><img src="PLAYLIST_IMAGE_URL" style="max-height: 1rem; max-width: 1rem;" /></span>
                            PLAYLIST_NAME
                        </label>
                        </div>
                    </li>
                `;

                data.forEach((playlist) => {
                    htmlContent += htmlTemplate
                        .replace(/PLAYLIST_ID/g, playlist["id"])
                        .replace(/PLAYLIST_NAME/g, playlist["name"])
                        .replace(/PLAYLIST_IMAGE_URL/g, playlist["imageUrl"]);
                });
                containerElement.innerHTML = htmlContent;
            })
            .catch(function (err) {
                console.warn("Something went wrong.", err);
                htmlContent =
                    '<div class="alert alert-warning">Error fetching API data.</div>';
                containerElement.innerHTML = htmlContent;
            });
    }
};

/* get artists and update form */
window.getTopArtists = function () {
    var containerElement = document.getElementById("user-top-artists");
    if (containerElement) {
        var htmlContent = "";

        var apiUrl = "artists/top";
        fetch(apiUrl)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                const htmlTemplate = `
                  <li class="col">
                    <div class="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        class="custom-control-input checkbox-artist"
                        id="check-top-artist-ARTIST_ID"
                        name="selected-artist[]"
                        value="ARTIST_ID"
                      >
                      <label class="custom-control-label" for="check-top-artist-ARTIST_ID">
                        <span class="spotify-image"><img src="ARTIST_IMAGE_URL" style="max-height: 1rem; max-width: 1rem;" /></span>
                        ARTIST_NAME
                      </label>
                    </div>
                  </li>
                `;

                data.forEach((artist) => {
                    htmlContent += htmlTemplate
                        .replace(/ARTIST_ID/g, artist["id"])
                        .replace(/ARTIST_NAME/g, artist["name"])
                        .replace(/ARTIST_IMAGE_URL/g, artist["imageUrl"]);
                });
                containerElement.innerHTML = htmlContent;
            })
            .catch(function (err) {
                console.warn("Something went wrong.", err);
                htmlContent =
                    '<div class="alert alert-warning">Error fetching API data.</div>';
                containerElement.innerHTML = htmlContent;
            });
    }
};

getPlaylists();
getTopArtists();
