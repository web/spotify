## Register app

<https://developer.spotify.com/dashboard/applications>

App name:
    Spotify playlist manager

Add description:
    Application to help create/update playlists from other playlists or selected artists/tracks.
    Something like "radios", but more personalized.

Client ID:
    41597dbeb0b3403caa28e7e428e0c9b2

Client Secret:
    <hidden>
