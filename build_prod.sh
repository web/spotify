#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

if [[ -f "${CURRENT_DIR}/.env.local" ]]; then
  sed -i "s|^APP_ENV=dev$|APP_ENV=prod|g" "${CURRENT_DIR}/.env.local"
fi

composer install --no-ansi --no-interaction --no-plugins --no-progress --no-scripts --optimize-autoloader
php bin/console cache:clear --env=prod

yarn install
yarn encore production
